#!/bin/bash

date=$(date)

echo "build"
mdbook build
echo "push"
git add *
git commit -s -m "new version from $date"
git push origin master
echo "done!"
