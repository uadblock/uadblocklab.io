# What is DNS?

DNS is the domain name system. It is used to assign names to IP's and vice versa. The normal user on the Internet calls a website via a domain, for example **ubuntu.com**. However, the servers are reached on the Internet via IP addresses.

To perform this step and obtain an IP address for each domain, each system, cell phone or computer, asks for a DNS server. The following graphic briefly illustrates this step:

![DNS](dns.png)
