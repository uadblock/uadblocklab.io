# <i class="fa fa-shield"></i> uAdBlock
 
 <i class="fa fa-shield"></i> **uAdBlock** is an adblocker based on blocklists. These blocklists come from different sources and prevent the loading of advertisements, scams and phishing sites as well as other unwanted content.

In this documentation you will find help on how to use the app as well as information about the lists used. Additionally, the basic principle behind the app is explained.

<i class="fa fa-shield"></i> **uAdblock** is an app exclusively for the mobile operating system **Ubuntu Touch**.

#### Links

- [Source Code](https://gitlab.com/uadblock/uadblock)
- [Issue Tracker](https://gitlab.com/uadblock/uadblock/issues)
- [Open-Store](https://open-store.io/app/uadblock.mariogrip)
