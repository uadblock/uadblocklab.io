# Blocklists

Blocklists are the basis for blocking advertising. As explained earlier in the documentation, these block lists are based on simple entries that look like this:

```
0.0.0.0	example.com
0.0.0.0 example2.com
0.0.0.0	example3.com
...
```
In the other chapters, you will find information about all blocklists used in uAdBlock.
When using multiple lists, uAdblock ensures that no duplicate entries are made so that the host file is as lean and perfomant as possible.
