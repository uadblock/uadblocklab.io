# How does blocking content work?

When you read the What's DNS page, you know how DNS works.
In this article was on a trifle not entered. Each system checks, before the DNS server asks for the IP address, first of all its own host list, if the domain is known. This step is performed on every request.

And that's exactly where uAdBlock starts. We replace the system-appropriate hostlist with the blocklists. If the user, you, is surfing the internet and visiting a web page, the system will check if there is that domain in the host file, if so, it will return an IP that can not be resolved, in our case 0.0.0.0. This will not load the content.

If you go to a page this page but not blocked, it will be loaded. On this page, however, contents of other DOmains should be retrieved, such as Advertisements will be reviewed again and then only these items will be blocked.

This makes the page call much faster and most pages basically clearer.
